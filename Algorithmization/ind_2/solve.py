def no_perms_digits(n):
    res = 1

    for i in range(2, n+1):
        res *= i
    
    return len(str(res))

