import Test
from solve import binary_to_decimal

Test.assert_equals(binary_to_decimal([1, 1, 1, 1, 1, 1, 1, 1]),             255)
Test.assert_equals(binary_to_decimal([0, 0, 0, 0, 0, 0, 0, 0]),               0)
Test.assert_equals(binary_to_decimal([1, 0, 1, 1, 1, 1, 0, 0]),             188)
Test.assert_equals(binary_to_decimal([1, 0, 1, 1, 0, 1, 0, 1]),             181)
Test.assert_equals(binary_to_decimal([1, 0, 0, 1, 1, 0, 1]),                 77)
Test.assert_equals(binary_to_decimal([1, 0, 1, 1, 0, 1, 1, 1, 0, 0, 1, 1]), 291)
Test.assert_equals(binary_to_decimal([1, 0, 1, 1, 0, 1, 0, 1]),              12)
Test.assert_equals(binary_to_decimal([1, 1, 1, 1, 0, 0, 0, 0]),              22)
Test.assert_equals(binary_to_decimal([1, 0, 0, 0, 0, 0, 0, 0]),               1)
Test.assert_equals(binary_to_decimal([1, 0, 0, 0, 1, 0, 0, 1]),              54)