def binary_to_decimal(arr):
    st = 1
    ans = 0

    for i in range(len(arr) - 1, -1, -1):
        ans += st * arr[i]
        st *= 2
    
    return ans
