def pi_15_approximations(k):
    n = 2
    pi = 3

    for i in range(2, 2 + k):
        if (i % 2 == 0):
            pi += 4 / (n * (n + 1) * (n + 2))
        else: 
            pi -= 4 / (n * (n + 1) * (n + 2))
        n += 2
    
    template = '{:.' + str(20) + 'f}'
    print(template.format(pi))

pi_15_approximations(15)
pi_15_approximations(300)
pi_15_approximations(1500)
pi_15_approximations(30000)
pi_15_approximations(100000)