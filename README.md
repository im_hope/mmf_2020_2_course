#### Вычислительная практика_Фролова Надежда, 2 курс, 8 группа

#### Индивидуальные задания:
1. [Алгоритмизация 1_10](https://bitbucket.org/im_hope/mmf_2020_2_course/src/master/Algorithmization/ind_1/) 
2. [Алгоритмизация 2_4](https://bitbucket.org/im_hope/mmf_2020_2_course/src/master/Algorithmization/ind_2/)
3. [Алгоритмизация 3_1](https://bitbucket.org/im_hope/mmf_2020_2_course/src/master/Algorithmization/ind_3/)

#### Групповое задание: 
1. [Веб-страница](https://bitbucket.org/im_hope/mmf_2020_2_course/src/master/Group%20webpage/)
2. [Визуализация](https://bitbucket.org/im_hope/mmf_2020_2_course/src/master/Group%20visualization/)
2. [Результат работы](https://bookingbyegor.herokuapp.com/)




