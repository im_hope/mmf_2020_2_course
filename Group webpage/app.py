from datetime import datetime
import timeit
import time 
from flask import Flask
from flask import render_template
from flask import Flask, redirect, url_for, render_template, request



app = Flask(__name__)


file = "out10.csv"
dataFormat = '%Y-%m-%d %H:%M:%S'


def parseUserInputWithID(ResourseID, StartDateTime, EndDateTime):
    
    StartDateTime = StartDateTime.replace('T', ' ')[:StartDateTime.rfind('.')]
    StartDateTime = datetime.strptime(StartDateTime, dataFormat)
    StartDateTime = int(time.mktime(StartDateTime.timetuple())) // 100

    EndDateTime = EndDateTime.replace('T', ' ')[:EndDateTime.rfind('.')]
    EndDateTime = datetime.strptime(EndDateTime, dataFormat)
    EndDateTime = int(time.mktime(EndDateTime.timetuple())) // 100

    return [ResourseID, StartDateTime, EndDateTime]


def parseUserInput(StartDateTime, EndDateTime):

    StartDateTime = StartDateTime.replace('T', ' ')[:StartDateTime.rfind('.')]
    StartDateTime = datetime.strptime(StartDateTime, dataFormat)
    StartDateTime = int(time.mktime(StartDateTime.timetuple())) // 100

    
    EndDateTime = EndDateTime.replace('T', ' ')[:EndDateTime.rfind('.')]
    EndDateTime = datetime.strptime(EndDateTime, dataFormat)
    EndDateTime = int(time.mktime(EndDateTime.timetuple())) // 100

    return [StartDateTime, EndDateTime]


def read(file):
    f = open(file)
    
    data = f.read()

    f.close()

    return data


def parseString(str):
    return str.split(",")


def printArray(arr):
    for i in arr:
        print(i)


def parseInput(data, dataFormat):

    dict = {}

    data = data.split('\n')
        
    for i in range(1, len(data) - 1):

        data[i] = data[i].split(',')
        data[i][1] = data[i][1].replace('T', ' ')[:data[i][1].rfind('.')]
        data[i][1] = datetime.strptime(data[i][1], dataFormat)

        data[i][2] = data[i][2].replace('T', ' ')[:data[i][2].rfind('.')]
        data[i][2] = datetime.strptime(data[i][2], dataFormat)

        #делим на 100 --> меленькие числа быстрее сравниваются
        unix1 = int(time.mktime(data[i][1].timetuple())) // 100
        unix2 = int(time.mktime(data[i][2].timetuple())) // 100

        st = str(unix1) + "," + str(unix2)

        if data[i][0] in dict:
            dict.update({data[i][0]:dict[data[i][0]] + "," + st})
        else:
            dict.update({data[i][0]:st})
       
    return dict


dict = parseInput(read(file), dataFormat)


@app.route("/", methods=["POST", "GET"])
def login():
    if request.method == "POST":
        id = request.form.get("nm")
        start = request.form.get("nm1")
        end = request.form.get("nm2")

        if (id): 
            return redirect(url_for("findById", ResourseID=id, StartDateTime=start, EndDateTime = end))
        else:
            return redirect(url_for("searchId",StartDateTime=start, EndDateTime = end))
    else:
        return render_template("hotel.html", ans = "")


@app.route('/searchId/<StartDateTime>&<EndDateTime>', methods=["POST", "GET"])
def searchId(StartDateTime, EndDateTime):
    ids = set()
    closed = set()

    tmp = parseUserInput(StartDateTime, EndDateTime)

    for i in dict:
        arr = dict[str(i)].split(",")
        for val in range(0, len(arr), 2):
            if (i in closed) or not check([int(arr[val]), int(arr[val + 1])], tmp[0], tmp[1]):
                closed.add(i)
                if val in ids:
                    ids.remove(i)
            else:
                ids.add(i)
    str1= " "
    for i in ids:
        str1 += i + ","
    str1= str1[:-1]
    return  render_template("hotel.html", ans = str1) 


@app.route('/findById/<ResourseID>&<StartDateTime>&<EndDateTime>', methods=["POST", "GET"])
def findById(ResourseID, StartDateTime, EndDateTime):
    tmp = parseUserInputWithID(ResourseID, StartDateTime, EndDateTime)
    if tmp[0] in dict: 
        arr = dict[tmp[0]].split(",")
    else: 
        render_template("hotel.html", ans = "Занято")

    ans = "Свободно"
    
    for i in range(0, len(arr), 2):
        if (check([int(arr[i]), int(arr[i + 1])], tmp[1], tmp[2])):
            ans = "Занято"
            break
    return render_template("hotel.html", ans = ans) 


def check(hotel, dateStart, dateEnd):
  return (
    (hotel[0] <= dateStart < hotel[1]) 
    or  
    (hotel[0] < dateEnd <= hotel[1]) 
    or
    (dateStart <= hotel[0] < dateEnd) 
    or
    (dateStart < hotel[1] <= dateEnd)
  )


if __name__ == "__main__":
    app.run(debug = True)
